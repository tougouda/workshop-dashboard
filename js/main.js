/* La période de changement de page (en millisecondes) pat défaut */
var PERIOD = 30000;

/* Le décalage temporel (en millisecondes) par défaut */
var DELAY = 0;

function Slider(id, period, delay)
{
    this.tabBodyPages = document.querySelectorAll('#' + id + ' .machines.paging.dynamic > .body > .page');
    this.tabPages = document.querySelectorAll('#' + id + ' .machines.paging.dynamic > .pages > .page');

    this.activePage = 0;
    this.period = (period || PERIOD);
    this.delay = (delay || DELAY);

    if (this.tabBodyPages.length != this.tabPages.length)
    {
        console.error('Le nombre de pages est incohérent.');
        return;
    }

    if (this.tabPages.length == 1)
    {
        return;
    }

    this.displayPage = function(num)
    {
        this.tabBodyPages[num].classList.remove('hidden');
        this.tabPages[num].classList.add('active');
    }

    this.hidePage = function(num)
    {
        this.tabBodyPages[num].classList.add('hidden');
        this.tabPages[num].classList.remove('active');
    }

    this.activatePage = function(num)
    {
        var nbPages = this.tabPages.length;
        for (var i = 0; i < nbPages; i++)
        {
            if (i == num)
            {
                this.displayPage(i);
            }
            else
            {
                this.hidePage(i);
            }
        }
    
        this.activePage = num;
    }

    this.activateNextPage = function()
    {
        this.activatePage(this.activePage == this.tabPages.length - 1 ? 0 : this.activePage + 1);
        var mySelf = this;
        setTimeout(function() { mySelf.activateNextPage(); }, this.period);
    }

    // Activation de la première page
    this.activatePage(this.activePage);

    // Lancement de la rotation des pages
    var mySelf = this;
    setTimeout(function() { mySelf.activateNextPage(); }, this.delay);
};


new Slider('allocations', PERIOD, PERIOD);
new Slider('to-prepare', PERIOD, PERIOD + 500);
new Slider('recoveries', PERIOD, PERIOD + 1000);
