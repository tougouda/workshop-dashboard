# Console de supervision atelier

Maquette HTML pour un projet de console de supervision dans un atelier de préparation de machines.

![Capture d’écran](img/screenshot.png)

La page est destinée à être affichée sur une TV LED 123 cm placée en hauteur dans l’atelier de chaque site.

L’idée est que les données brutes soient collectées depuis un certain nombre d’applications métiers puis retravaillées et stockées dans une base de données propre à la console de supervision. Les données sont ensuite poussées à chacun des clients via [*Server-Sent Events*](https://fr.wikipedia.org/wiki/Server-sent_events).