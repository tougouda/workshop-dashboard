<?php
$cool = (isset($_REQUEST['cool']) ? (bool) $_REQUEST['cool'] : false);

setlocale(LC_ALL, 'fr_FR');
$now = new \DateTime();
$update = clone $now;
$update->sub(new DateInterval('PT' . rand(0, 136) . 'M'));
?>
<html>
<head>
    <meta charset="utf-8" /> 
    <title>Panneau de contrôle atelier</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="img/favicons/DSH_favicon_16x16.png?__v=3.exp" />
    <link rel="icon" type="image/png" href="favicons/DSH_favicon_32x32.png?__v=3.exp" />
    <link rel="icon" type="image/png" href="favicons/DSH_favicon_128x128.png?__v=3.exp" />
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="img/favicons/DSH_favicon.ico?__v=3.exp" />
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>

<body>
    <div class="background"></div>
    <div class="content">
        <header>
            <div class="logo"></div>
            <div class="agency">Atelier Chambéry</div>
            <div class="date"><?php echo strftime('%a %x', $now->format('U')); ?></div>
            <div class="time"><?php echo $now->format('H:i'); ?></div>
        </header>
        <section>
            <div id="allocations" class="block allocations">
                <?php include('blocks/allocations.html'); ?>
            </div>
            <div id="to-prepare" class="block to-prepare">
                <?php include('blocks/to-prepare.html'); ?>
            </div>
            <div id="recoveries" class="block recoveries">
                <?php include('blocks/recoveries.html'); ?>
            </div>
        </section>
        <footer>Dernière mise à jour à <?php echo $update->format('H:i'); ?></footer>
    </div>
</body>

<script type="text/javascript" src="js/main.js"></script>
</html>
