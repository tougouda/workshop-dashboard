<html>
<head>
    <meta charset="utf-8" /> 
    <title>Légende du panneau de contrôle atelier</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="img/favicons/DSH_favicon_16x16.png?__v=3.exp" />
    <link rel="icon" type="image/png" href="favicons/DSH_favicon_32x32.png?__v=3.exp" />
    <link rel="icon" type="image/png" href="favicons/DSH_favicon_128x128.png?__v=3.exp" />
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="img/favicons/DSH_favicon.ico?__v=3.exp" />
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>

<body>
    <div class="content">
        <dl class="legend">
            <dt><div class="counter"><div class="problems">3</div></div></dt><dd class="text50">Problèmes d’attribution</dd>
            <dt><div class="counter"><div class="number"><div class="secondary">1</div></div></div></dt><dd class="text50">Livraisons anticipées possibles</dd>
            <dt><div class="flag color1">A</div></dt><dd class="text50">Machine en récupération pouvant être réattribuée</dd>
            <dt><div class="level">2</div></dt><dd class="text50">Niveau de révision ouvert</dd>
            <dt><i class="icon wanted"></i></dt><dd class="text50">Machine souhaitée</dd>
            <dt><i class="icon anticipated"></i></dt><dd class="text50">Livraison anticipée possible</dd>
            <dt><i class="icon docs"></i></dt><dd class="text50">Documents à fournir</dd>
            <dt><i class="icon electrical"></i></dt><dd class="text50">Machine électrique</dd>
            <dt><i class="icon duration"></i></dt><dd class="text50">Durée de location</dd>
            <dt><i class="icon asbestos"></i></dt><dd class="text50">Chantier amiante</dd>
            <dt><i class="icon transfer"></i></dt><dd class="text50">Reprise sur chantier</dd>
            <dt><i class="icon outside"></i></dt><dd class="text50">Durée hors de l’agence</dd>
            <dt><i class="icon inventory"></i></dt><dd class="text50">État des lieux effectué</dd>
            <dt><i class="icon timer"></i></dt><dd class="text50">Compteur horaire relevé</dd>
            <dt><div class="avatar color1">AB</div></dt><dd class="text50">Transporteur interne</dd>
            <dt><div class="avatar">A</div></dt><dd class="text50">Transporteur externe</dd>
            <dt><div class="avatar color2"><i class="icon self"></i></div></dt><dd class="text50">Enlèvement agence</dd>
        </dl>
    </div>
</body>

<script type="text/javascript" src="js/main.js"></script>
</html>
